# Overview

This repo contains a simple NodeJS application that runs through a Bitbucket pipeline that will demonstrate the [Dynatrace Push Event](https://bitbucket.org/dynatrace/dynatrace-push-event) and [Keptn Quality Gate](https://bitbucket.org/keptn/keptn-quality-gate/) official [Atlassian Bitbucket pipes](https://bitbucket.org/product/features/pipelines/integrations).

# Demo App

The demo app is a simple NodeJS that has a few endpoints to send traffic and to simulate issues that the Keptn Quality Gate and Dynatrace can detect. 

![Demo App](readme-images/app.png)

The application internally has logic to refer a version number read in from the `version` file or set with the API.  The version number will to produce these behaviors:

* _**version 1**_ = no issues
* _**version 2**_ = 50% of the calls to the ```/api/invoke``` endpoint will fail
* _**version 3**_ = all calls to the ```/api``` endpoint will have a forced response time delay. This time delay can be adjusted by calling this endpoint ```/api/sleeptime?min=xxxx``` where xxxx is number between 0 and 10000ms.
* _**version 4**_ = no issues

For demonstration, we can just adjust the ```version``` file and check it back into this repo. The pipeline will then create a new Docker image with this convention ```$DOCKER_REGISTRY_USERNAME/simplenodeservice:VERSION```. 

While the application is running, the endpoint ```/api/version?version=x```, can be called for an in-place "fake-out" for the running version.

_Demo app credit goes to Andi Grabner: https://github.com/grabnerandi/simplenodeservice_

### Pipeline overview

![Pipeline](readme-images/pipeline-flow.png)

1. Developer make a code change and checks in SLO/SLI files
1. Build and push the demo application Docker image and push to DockerHub
1. Deploy the demo application to Kubernetes using the [Atlassian eks kubectl pipe](https://bitbucket.org/atlassian/aws-eks-kubectl-run)
1. Push a Dynatrace deployment event using the [Dynatrace push event pipe](https://bitbucket.org/dynatracedemos/dynatrace-push-event)
1. Call a simple script that sends traffic to the demo application using curl commands 
1. Push Dynatrace Load Test annotation event using the [Dynatrace push event pipe](https://bitbucket.org/dynatracedemos/dynatrace-push-event)
1. Call Keptn Quality Gate using the [Keptn Quality Gate pipe](https://bitbucket.org/dynatracedemos/keptn-quality-gate)
1. Evaluate whether to pass or fail the pipeline based on the SLO validation.

# Dynatrace Events

[Dynatrace Events](https://www.dynatrace.com/support/help/extend-dynatrace/dynatrace-api/environment-api/events/post-event/) inform Dynatrace's AI deterministic root cause problem detection engine as well as provide hyperlinks and immediate context back to Bitbucket and Keptn with event cards in the Dynatrace web console.

Here is an example `CUSTOM_DEPLOYMENT` event with the details of the version and build.

![Event](readme-images/event.png)

# Keptn Quality Gate

This step uses the [Keptn Quality Gate pipe](https://bitbucket.org/keptn/keptn-quality-gate/).  It will process the SLI and SLO files found in the `keptn/` folder of this repo.

Here is a view of the bitbucket labels added by the start evaluation call.

![keptn-labels](readme-images/keptn-labels.png)

Here is a view of the quality gate build-over-build results in the Keptn Bridge.

![Gate](readme-images/lighthouse.png)

Learn more about Keptn on the [Keptn website](https://bitbucket.org/keptn/keptn-quality-gate/)

# Demo Setup

See the [SETUP README](SETUP.md) for detailed setup instructions