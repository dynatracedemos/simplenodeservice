var https = require('https');
var querystring = require('querystring');

exports.handler = (event, context, callback) => {
    var statusCode;
    var resultMsg;
    var postData = JSON.stringify(event);
    console.log('Request postData: ' + postData);
    
    var params = {
              host: process.env.KEPTN_HOST,
              rejectUnauthorized: false, 
              path: '/v1/event',
              method: 'POST',
              headers: {
                    'Content-Type': 'application/json',
                    'x-token': process.env.KEPTN_TOKEN,
                    'Content-Length': Buffer.byteLength(postData)
                }
              };
  
    var req = https.request(params, function(res) {
      let data = '';
      console.log('Calling: https://' + params.host + params.path);
    
      req.on('error', (error) => {
        statusCode = "500";
        resultMsg = 'Error: ' + error.message
      	console.log(resultMsg);
      });
      
      res.setEncoding('utf8');
      res.on('data', function(chunk) {
        data += chunk;
      });
      res.on('end', function() {

        // return response
        var response = {
          statusCode: res.statusCode,
          headers : { "content-type" : "application/json" },
          body: JSON.parse(data)
        };
        
        console.log('Response : ' + JSON.stringify(response));
        callback(null, response);
      });
    });
    
    req.write(postData);
    req.end();

 };
 
 