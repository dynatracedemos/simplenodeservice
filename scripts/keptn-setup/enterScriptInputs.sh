#!/bin/bash

CREDS=./creds.json

DEPLOYMENT=eks
rm ./creds.json

if [ -f "$CREDS" ]
then
    DT_ENVIRONMENT_ID=$(cat creds.json | jq -r '.dynatraceEnvironmentId')
    DT_API_TOKEN=$(cat creds.json | jq -r '.dynatraceApiToken')
    DT_PAAS_TOKEN=$(cat creds.json | jq -r '.dynatracePaaSToken')

    REPO_PERSONAL_ACCESS_TOKEN=$(cat creds.json | jq -r '.repoPersonalAccessToken')
    REPO_USER_NAME=$(cat creds.json | jq -r '.repoUserName')
    REPO_REMOTE_URL=$(cat creds.json | jq -r '.repoRemoteUrl')

    AKS_SUBSCRIPTION_ID=$(cat creds.json | jq -r '.aksSubscriptionId')
    AKS_LOCATION=$(cat creds.json | jq -r '.aksLocation')

    GKE_PROJECT=$(cat creds.json | jq -r '.gkeProject')
    GKE_CLUSTER_ZONE=$(cat creds.json | jq -r '.gkeClusterZone')
    GKE_CLUSTER_REGION=$(cat creds.json | jq -r '.gkeClusterRegion')

    EKS_CLUSTER_REGION=$(cat creds.json | jq -r '.eksClusterRegion')
    EKS_CLUSTER_NAME=$(cat creds.json | jq -r '.eksClusterName')
fi

echo "==================================================================="
echo -e "Please enter the values for provider type: $DEPLOYMENT_NAME:"
echo "==================================================================="
echo "Dynatrace Environment ID (e.g. https://ENVIRONMENTID.live.dynatrace.com)"
read -p "                                       (current: $DT_ENVIRONMENT_ID) : " DT_ENVIRONMENT_ID_NEW
read -p "Dynatrace API Token                    (current: $DT_API_TOKEN) : " DT_API_TOKEN_NEW
read -p "Dynatrace PaaS Token                   (current: $DT_PAAS_TOKEN) : " DT_PAAS_TOKEN_NEW
read -p "Repo User Name                         (current: $REPO_USER_NAME) : " REPO_USER_NAME_NEW
read -p "Repo Personal Access Token             (current: $REPO_PERSONAL_ACCESS_TOKEN) : " REPO_PERSONAL_ACCESS_TOKEN_NEW
read -p "Repo Remote URL                        (current: $REPO_REMOTE_URL) : " REPO_REMOTE_URL_NEW

case $DEPLOYMENT in
  eks)
    read -p "AWS Cluster Region (eg.us-east-1)      (current: $EKS_CLUSTER_REGION) : " EKS_CLUSTER_REGION_NEW
    read -p "AWS Cluster Name                       (current: $EKS_CLUSTER_NAME) : " EKS_CLUSTER_NAME_NEW
    ;;
  aks)
    read -p "Azure Subscription ID                  (current: $AKS_SUBSCRIPTION_ID) : " AKS_SUBSCRIPTION_ID_NEW
    read -p "Azure Location                         (current: $AKS_LOCATION) : " AKS_LOCATION_NEW
    ;;
  gke)
    read -p "Google Project                         (current: $GKE_PROJECT) : " GKE_PROJECT_NEW
    read -p "Google Cluster Zone (eg.us-east1-b)    (current: $GKE_CLUSTER_ZONE) : " GKE_CLUSTER_ZONE_NEW
    read -p "Google Cluster Region (eg.us-east1)    (current: $GKE_CLUSTER_REGION) : " GKE_CLUSTER_REGION_NEW
    ;;
  ocp)
    ;;
esac
echo "==================================================================="
echo ""
# set value to new input or default to current value
KEPTN_BRANCH=${KEPTN_BRANCH_NEW:-$KEPTN_BRANCH}
RESOURCE_PREFIX=${RESOURCE_PREFIX_NEW:-$RESOURCE_PREFIX}
DT_ENVIRONMENT_ID=${DT_ENVIRONMENT_ID_NEW:-$DT_ENVIRONMENT_ID}
DT_API_TOKEN=${DT_API_TOKEN_NEW:-$DT_API_TOKEN}
DT_PAAS_TOKEN=${DT_PAAS_TOKEN_NEW:-$DT_PAAS_TOKEN}
REPO_USER_NAME=${REPO_USER_NAME_NEW:-$REPO_USER_NAME}
REPO_PERSONAL_ACCESS_TOKEN=${REPO_PERSONAL_ACCESS_TOKEN_NEW:-$REPO_PERSONAL_ACCESS_TOKEN}
REPO_REMOTE_URL=${REPO_REMOTE_URL_NEW:-$REPO_REMOTE_URL}
# eks specific
EKS_CLUSTER_REGION=${EKS_CLUSTER_REGION_NEW:-$EKS_CLUSTER_REGION}
EKS_CLUSTER_NAME=${EKS_CLUSTER_NAME_NEW:-$EKS_CLUSTER_NAME}
# aks specific
AKS_SUBSCRIPTION_ID=${AKS_SUBSCRIPTION_ID_NEW:-$AKS_SUBSCRIPTION_ID}
AKS_LOCATION=${AKS_LOCATION_NEW:-$AKS_LOCATION}
# gke specific
GKE_PROJECT=${GKE_PROJECT_NEW:-$GKE_PROJECT}
GKE_CLUSTER_ZONE=${GKE_CLUSTER_ZONE_NEW:-$GKE_CLUSTER_ZONE}
GKE_REGION_ZONE=${GKE_REGION_NEW:-$GKE_REGION_ZONE}

echo -e "Please confirm all are correct:"
echo ""
echo "Dynatrace Host Name          : $DT_ENVIRONMENT_ID"
echo "Dynatrace API Token          : $DT_API_TOKEN"
echo "Dynatrace PaaS Token         : $DT_PAAS_TOKEN"
echo "Repo User Name               : $REPO_USER_NAME"
echo "Repo Personal Access Token   : $REPO_PERSONAL_ACCESS_TOKEN"
echo "Repo Remote URL              : $REPO_REMOTE_URL" 

case $DEPLOYMENT in
  eks)
    echo "AWS Cluster Region           : $EKS_CLUSTER_REGION"
    echo "AWS Cluster Name             : $EKS_CLUSTER_NAME"
    ;;
  aks)
    echo "Azure Subscription ID        : $AKS_SUBSCRIPTION_ID"
    echo "Azure Location               : $AKS_LOCATION"
    ;;
  gke)
    echo "Google Project               : $GKE_PROJECT"
    echo "Google Cluster Zone          : $GKE_CLUSTER_ZONE"
    echo "Google Cluster Region        : $GKE_CLUSTER_REGION"
    ;;
  ocp)
    ;;
esac
echo "==================================================================="
read -p "Is this all correct? (y/n) : " -n 1 -r
echo ""
echo "==================================================================="

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Making a backup $CREDS to $CREDS.bak"
    cp $CREDS $CREDS.bak 2> /dev/null
    rm $CREDS 2> /dev/null

    cat ./creds.template | \
      sed 's~DEPLOYMENT_PLACEHOLDER~'"$DEPLOYMENT"'~' | \
      sed 's~KEPTN_BRANCH_PLACEHOLDER~'"$KEPTN_BRANCH"'~' | \
      sed 's~DYNATRACE_HOSTNAME_PLACEHOLDER~'"$DT_ENVIRONMENT_ID"'~' | \
      sed 's~DYNATRACE_API_TOKEN_PLACEHOLDER~'"$DT_API_TOKEN"'~' | \
      sed 's~DYNATRACE_PAAS_TOKEN_PLACEHOLDER~'"$DT_PAAS_TOKEN"'~' | \
      sed 's~REPO_USER_NAME_PLACEHOLDER~'"$REPO_USER_NAME"'~' | \
      sed 's~REPO_PERSONAL_ACCESS_TOKEN_PLACEHOLDER~'"$REPO_PERSONAL_ACCESS_TOKEN"'~' | \
      sed 's~REPO_REMOTE_URL_PLACEHOLDER~'"$REPO_REMOTE_URL"'~' > $CREDS

    case $DEPLOYMENT in
      eks)
        cp $CREDS $CREDS.temp
        cat $CREDS.temp | \
          sed 's~EKS_CLUSTER_NAME_PLACEHOLDER~'"$EKS_CLUSTER_NAME"'~' | \
          sed 's~EKS_CLUSTER_REGION_PLACEHOLDER~'"$EKS_CLUSTER_REGION"'~' > $CREDS
        rm $CREDS.temp 2> /dev/null
        ;;
      aks)
        cp $CREDS $CREDS.temp
        cat $CREDS.temp | \
          sed 's~AKS_SUBSCRIPTION_ID_PLACEHOLDER~'"$AKS_SUBSCRIPTION_ID"'~' | \
          sed 's~AKS_LOCATION_PLACEHOLDER~'"$AKS_LOCATION"'~' > $CREDS
        rm $CREDS.temp 2> /dev/null
        ;;
      gke)
        cp $CREDS $CREDS.temp
        cat $CREDS.temp | \
          sed 's~GKE_PROJECT_PLACEHOLDER~'"$GKE_PROJECT"'~' | \
          sed 's~GKE_CLUSTER_REGION_PLACEHOLDER~'"$GKE_CLUSTER_REGION"'~' | \
          sed 's~GKE_CLUSTER_ZONE_PLACEHOLDER~'"$GKE_CLUSTER_ZONE"'~' > $CREDS
        rm $CREDS.temp 2> /dev/null
        ;;
      ocp)
        ;;
    esac
    echo ""
    echo "The updated credentials file can be found here: $CREDS"
    echo ""
fi