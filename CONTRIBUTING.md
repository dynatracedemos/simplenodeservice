# Contributing to Bitbucket Pipes

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

# Developer Notes

Use and adjust these commands as need to build, test, and push the demo app locally.

```
# build and run
export TAG=`echo "$(cat version)" | awk 1`
docker build -t [repo]/simplenodeservice:$TAG . && docker run -p 8080:8080 dtdemos/simplenodeservice:$TAG

# build and push
export TAG=`echo "$(cat version)" | awk 1`
docker build -t [repo]/simplenodeservice:$TAG . && docker push [repo]/simplenodeservice:$TAG
```

if you add the JIRA story/big number in the commit message (e.g. `[DD-1]`), then it will link back to JIRA item.