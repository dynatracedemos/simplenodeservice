# Setup Overview

These instructions were developed with this setup:

* Dockerhub - account with credentials to push an image within pipeline
* AWS Kuberneters EKS - version 1.15 Cluster on AWS
* AWS user with API access - need the access and secret key to call kubectl within pipeline
* [Dynatrace tenant](https://www.dynatrace.com/trial/) with a [Dynatrace API token](https://keptn.sh/docs/0.6.0/reference/m onitoring/dynatrace/#setup-dynatrace)
* An empty bitbucket repo to be used as a Keptn upstream repo and a Bitbucket user ID and an App password. See [keptn docs for details](https://keptn.sh/docs/0.5.0/manage/project/#bitbucket)
* [Keptn 0.6.1](https://keptn.sh) installed within the cluster
* Slack and MS teams webhook configured for the Keptn Notification service. See [Keptn Notification Reference Docs for details](https://github.com/keptn-contrib/notification-service) for details.

# Install Pre-requisite tools

Here are commands used to install everything on MacOS.

```
# jq json utility - https://github.com/stedolan/jq/releases
brew install jq

# aws cli 2.x (tested with aws-cli/2.0.7)
aws --version
curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /
rm AWSCLIV2.pkg

# eksctl (tested with 0.16.0) for provisioning the EKS cluster
eksctl version
brew tap weaveworks/tap
brew install weaveworks/tap/eksctl

# kubectl
kubectl version
curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/darwin/amd64/kubectl
chmod +x ./kubectl
mv kubectl /usr/local/bin/kubectl
```

# Gather your environment details

This script will prompt you for secrets needs for the installation.  
* Results are stored into `creds.json` file. 
* This file is excluded in `.gitignore` file.
* The scripts in the next sections will read the values from `creds.json`

```
cd scripts/keptn-setup
./enterScriptInputs.sh
```

# Provision Cluster 

Here are commands used to create the cluster and configure kubectl authentication

```
# get entered values from credentials file
EKS_CLUSTER_REGION=$(cat creds.json | jq -r '.eksClusterRegion')
EKS_CLUSTER_NAME=$(cat creds.json | jq -r '.eksClusterName')

# provision the cluster.  Monitor in AWS CloudFormation console stacks page
eksctl create cluster --version=1.15 --name=$EKS_CLUSTER_NAME --node-type=m5.2xlarge --nodes=1 --region=$EKS_CLUSTER_REGION

# connect kubectl once fully provisioned
aws eks --region $EKS_CLUSTER_REGION update-kubeconfig --name $EKS_CLUSTER_NAME

# test kubectl connection
kubectl get ns
```

*[Keptn Reference Docs](https://keptn.sh/docs/0.6.0/installation/setup-keptn/)*

# Install Keptn

## 1. Get Keptn CLI

Download and Install the CLI by running this script that will download and move the keptn binary to `/usr/local/bin/keptn`

```
curl -sL https://get.keptn.sh | sudo -E bash
```

Validate keptn CLI version is 0.6.1 with this command

```
keptn version
```

*[Keptn CLI Reference Docs](https://github.com/keptn/keptn/releases)*

## 2. Install keptn

Install Keptn for EKS

```
keptn install --platform=eks
```

## 3. Configure EKS domain
 
For AWS you need to setup the hosted domain and an alias record.  To get the target and configure keptn, run these commands.

```
MY_DOMAIN="$(kubectl get svc istio-ingressgateway -n istio-system -o jsonpath={.status.loadBalancer.ingress[0].hostname}")
echo "Configuring keptn domain to: $MY_DOMAIN"
keptn configure domain $MY_DOMAIN
```

Check if the installation was successful and also retrieve the public IP of your Keptn installation

```
keptn status
```

Output (similar to this):

```
Starting to authenticate
Successfully authenticated
CLI is authenticated against the Keptn cluster https://api.keptn.YOURDOMAIN
```

## 4. Expose Keptn Bridge

Use this script to expose Keptn bridge on your Keptn domain.  The script will output the URL that you can open in browser to verify access.  NOTE that no project will yet be available since we have not performed that step.

```
# Update to early-access version for latest features
kubectl -n keptn set image deployment/bridge bridge=keptn/bridge2:20200402.1046 --record

# expose bridge
./exposeBridge.sh
```

*[keptn reference docs](https://keptn.sh/docs/0.6.0/reference/keptnsbridge/#usage)*

## 5. Install Keptn Dynatrace Service

This with both install and configure the Dynatrace Keptn Service and Install the Dynatace OneAgent Operator.

```
# get entered values from credentials file
DT_ENVIRONMENT_ID=$(cat creds.json | jq -r '.dynatraceEnvironmentId')
DT_API_TOKEN=$(cat creds.json | jq -r '.dynatraceApiToken')
DT_PAAS_TOKEN=$(cat creds.json | jq -r '.dynatracePaaSToken')

# create k8 secret
kubectl -n keptn create secret generic dynatrace --from-literal="DT_TENANT=$DT_ENVIRONMENT_ID" --from-literal="DT_API_TOKEN=$DT_API_TOKEN"  --from-literal="DT_PAAS_TOKEN=$DT_PAAS_TOKEN"

# Install Dynatace Keptn Service
kubectl apply -f https://raw.githubusercontent.com/keptn-contrib/dynatrace-service/0.6.2/deploy/manifests/dynatrace-service/dynatrace-service.yaml

# This configures service and Install the Dynatace OneAgent Operator
keptn configure monitoring dynatrace
```

Use these commands to review your setup.

```
kubectl get svc dynatrace-service -n keptn
kubectl get pods -n dynatrace
```

Goto your Dynatrace web UI to verify deployment status.

*[keptn reference docs](https://keptn.sh/docs/0.6.0/reference/monitoring/dynatrace/#setup-dynatrace)*

## 6. Install Keptn Dynatrace SLI service

Run these commands to install the SLI service and configure the K8 secret that will be used by the Dynatrace SLI provider for the project. 

```
# install the Dynatace SLI provider k8 deployment, service, and distributors
./installDynatraceSLIService.sh

# create k8 secret for the project
KEPTN_PROJECT=$(cat creds.json | jq -r '.keptnProject')
./enableDynatraceSLIforProject.sh $KEPTN_PROJECT
```

*[Keptn Reference Docs](https://keptn.sh/docs/0.6.0/reference/monitoring/dynatrace/#setup-dynatrace-sli-provider)*

## 7. Install Keptn Notification Service (Optional)

This assumes you have either one or both Slack and MS teams application webhook configured.  See [Keptn Notification Reference Docs](https://github.com/keptn-contrib/notification-service) for details.

1. Run this command to get the manifest file.

    ```
    curl "https://raw.githubusercontent.com/keptn-contrib/notification-service/master/deploy/notification-service.yaml" -o "manifests/gen/notification-service.yaml"
    ```

1. edit the TEAMS_URL and BRIDGE_URL values in the the file

1. apply the manifest file

    ```
    kubectl apply -f manifests/gen/notification-service.yaml
    ```

# Onboard project to Keptn

## 1. Register project & service

Onboard project and service to Keptn required by Keptn Lighthouse service. 

This setup assumed, an empty bitbucket repo to be used as a Keptn upstream repo and a Bitbucket user ID and an App password.  [See keptn docs for details](https://keptn.sh/docs/0.5.0/manage/project/#bitbucket)

```
# get entered values from credentials file
REPO_PERSONAL_ACCESS_TOKEN=$(cat creds.json | jq -r '.repoPersonalAccessToken')
REPO_USER_NAME=$(cat creds.json | jq -r '.repoUserName')
REPO_REMOTE_URL=$(cat creds.json | jq -r '.repoRemoteUrl')
KEPTN_PROJECT=$(cat creds.json | jq -r '.keptnProject')

# create project
keptn create project $KEPTN_PROJECT --shipyard=./keptn-files/shipyard.yaml --git-user=$REPO_USER_NAME --git-token=$REPO_PERSONAL_ACCESS_TOKEN --git-remote-url=$REPO_REMOTE_URL

# create service
keptn create service simplenodeservice --project=$KEPTN_PROJECT
```

*[Reference Docs](https://keptn.sh/docs/0.6.0/usecases/quality-gates/#configure-keptn-and-activate-the-quality-gate)*

## 2. Register the project SLI and SLO files

Add the SLI with the custom tag rules since we don't want to use the default tagging. [Reference Docs](https://keptn.sh/docs/0.6.0/reference/monitoring/dynatrace/#configure-custom-slis)

```
# get entered values from credentials file
KEPTN_PROJECT=$(cat creds.json | jq -r '.keptnProject')

# add SLI resource
keptn add-resource --project=$KEPTN_PROJECT --stage=development --service=simplenodeservice --resource=./keptn-files/sli-development.yaml --resourceUri=dynatrace/sli.yaml
keptn add-resource --project=$KEPTN_PROJECT --stage=staging --service=simplenodeservice --resource=./keptn-files/sli-staging.yaml --resourceUri=dynatrace/sli.yaml
keptn add-resource --project=$KEPTN_PROJECT --stage=production --service=simplenodeservice --resource=./keptn-files/sli-production.yaml --resourceUri=dynatrace/sli.yaml

# add SLO resources
keptn add-resource --project=$KEPTN_PROJECT --stage=development --service=simplenodeservice --resource=./keptn-files/slo-development.yaml --resourceUri=slo.yaml
keptn add-resource --project=$KEPTN_PROJECT --stage=staging --service=simplenodeservice --resource=./keptn-files/slo-staging.yaml --resourceUri=slo.yaml
keptn add-resource --project=$KEPTN_PROJECT --stage=production --service=simplenodeservice --resource=./keptn-files/slo-production.yaml --resourceUri=slo.yaml
```

## 3. Setup us custom calculated service metrics 

```
# get entered values from credentials file
KEPTN_PROJECT=$(cat creds.json | jq -r '.keptnProject')
DT_API_TOKEN=$(cat creds.json | jq -r '.dynatraceApiToken')
DT_TENANT="$(cat creds.json | jq -r '.dynatraceEnvironmentId').live.dynatrace.com"

git clone git@github.com:grabnerandi/keptn-qualitygate-examples.git
cd keptn-qualitygate-examples/simpleservice/dynatrace

./createCalculatedMetrics.sh ENVIRONMENT application $KEPTN_PROJECT
./createTestStepCalculatedMetrics.sh ENVIRONMENT application $KEPTN_PROJECT
```

Verify in Dynatrace *(settings -> server-side service monitoring -> calculated service metrics)*

## 4. Create Namespaces

    ```
    kubectl create ns bitbucket-demo-development
    kubectl create ns bitbucket-demo-staging
    kubectl create ns bitbucket-demo-production
    ```

## 5. Review

Use these commands to review your setup.

    ```
    kubectl -n keptn get cm
    kubectl -n keptn get secret
    kubectl -n bitbucket-demo-development get pods
    kubectl -n bitbucket-demo-development get svc
    ```

Verify all SLO and SLI files are in the upstream BitBucket Repo

## 6. Manually Test the Keptn Quality Gate using these commands

```
# get entered values from credentials file
KEPTN_PROJECT=$(cat creds.json | jq -r '.keptnProject')

# send evaluation event (optional add start argument like 2020-03-10T00:42:44)
keptn send event start-evaluation --project=$KEPTN_PROJECT --service=simplenodeservice --stage=development --timeframe=5m

# check for status. Adjust with context ID from the send event command
keptn get event evaluation-done --keptn-context=[CONTEXT ID]
```

Then, check for evaluation status in Keptn Bridge

# Pipeline Setup

## Bitbucket pipeline repository variables

Set these as repository variables within the repository settings

```
# for wait for ready and send traffic scripts
CLUSTER_NAME            - eks cluster name

# for docker registry push 
DOCKER_REGISTRY_USERNAME  - use your repo credentials
DOCKER_REGISTRY_PASSWORD  - use your repo credentials

# for deployments
AWS_ACCESS_KEY_ID       - (see footnote below)
AWS_SECRET_ACCESS_KEY   - (see footnote below)
AWS_DEFAULT_REGION      - match cluster location, e.g. us-east-2

# for push events
DYNATRACE_BASE_URL      - e.g. https://[tenant].live.dynatrace.com
DYNATRACE_API_TOKEN     - use the API token for your tenant

# for keptn quality gate  
KEPTN_URL               -  (see footnote below)
KEPTN_TOKEN             -  (see footnote below)
```

_Footnotes_

_1. for KEPTN_URL and KEPTN_TOKEN, these commands can be used to get the values_

```
# KEPTN_URL
echo "https://api.keptn.$(kubectl get cm keptn-domain -n keptn -ojsonpath={.data.app_domain})"

# KEPTN_TOKEN
kubectl get secret keptn-api-token -n keptn -ojsonpath={.data.keptn-api-token} | base64 --decode
```
_2. AWS user for AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY needs to just have this permission policy_

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "eks:DescribeCluster",
            "Resource": "*"
        }
    ]
}
```

## Bitbucket pipeline deployments

The pipeline assumes the three deployments are created:

* development
* staging
* production

Each deployment has a environment variables called `APP_URL` that the bitbucket-pipeline.yml.  The deployment name is read from the built in variable: `$BITBUCKET_DEPLOYMENT_ENVIRONMENT`

This commands can be used to get the `APP_URL` value:

```
# APP_URL
echo "development = http://$(kubectl -n bitbucket-demo-development get service simplenodeservice -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'):8080"
echo "staging = http://$(kubectl -n bitbucket-demo-staging get service simplenodeservice -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'):8080"
echo "production = http://$(kubectl -n bitbucket-demo-production get service simplenodeservice -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'):8080"
```